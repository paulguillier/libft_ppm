# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/31 11:31:02 by pguillie          #+#    #+#              #
#    Updated: 2018/11/08 10:18:38 by pguillie         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME := libft_ppm.a

AR := ar

CC := gcc
WFLAGS := -Wall -Werror -Wextra
CFLAGS = $(WFLAGS)

LIBFT := libft/libft.a

INCDIR := includes/
SRCDIR := sources/
OBJDIR := objects/

HEADERS := $(addprefix $(INCDIR), \
	types/t_ppm.h \
	libft_ppm.h \
)

SOURCES := $(addprefix $(SRCDIR), \
	ft_ppm_alloc_row.c \
	ft_ppm_free_row.c \
	ft_ppm_dim.c \
	ft_ppm_plainformat.c \
	ft_ppm_maxval.c \
	ft_ppm_read_init.c \
	ft_ppm_read_quit.c \
	ft_ppm_read_row.c \
	ft_ppm_write_init.c \
	ft_ppm_write_row.c \
	ft_ppm_write_quit.c \
)

OBJECTS = $(SOURCES:$(SRCDIR)%.c=$(OBJDIR)%.o)

LIBOBJ = $(addprefix $(OBJDIR), $(shell $(AR) t $(LIBFT) | grep .o))

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): $(LIBFT) $(OBJDIR) $(OBJECTS)
	$(AR) rcs $@ $(OBJECTS)
	cp $(LIBFT) $(OBJDIR)
	cd $(OBJDIR) && $(AR) x $(notdir $(LIBFT))
	$(AR) rs $@ $(LIBOBJ)

$(OBJDIR)%.o: $(SRCDIR)%.c $(HEADERS) Makefile
	$(CC) $(CFLAGS) -I$(INCDIR) -c -o $@ $<

$(OBJDIR):
	mkdir -p $(OBJDIR)

$(LIBFT):
	make -C $(dir $(LIBFT))

clean:
	make -C $(dir $(LIBFT)) clean
	rm -rf $(OBJDIR)

fclean: clean
	make -C $(dir $(LIBFT)) fclean
	rm -f $(NAME)

re: fclean all
