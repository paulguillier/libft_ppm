/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_ppm.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/03 11:18:36 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/08 10:37:51 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PPM_H
# define LIBFT_PPM_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <stdint.h>

# include "types/t_ppm.h"
# include "libft/libft.h"

int			ft_ppm_read_init(char *file, t_ppm *data);
int			ft_ppm_read_row(t_ppm data, uint16_t **row);
int			ft_ppm_read_quit(t_ppm *data);

int			ft_ppm_write_init(char *file, t_ppm *data);
int			ft_ppm_write_row(t_ppm data, uint16_t **row);
int			ft_ppm_write_quit(t_ppm *data);

uint16_t	**ft_ppm_alloc_row(t_ppm data);
void		ft_ppm_free_row(uint16_t **row, t_ppm data);

int			ft_ppm_get_dim(t_ppm data, int *width, int *height);
int			ft_ppm_set_dim(t_ppm *data, int width, int height);

int			ft_ppm_get_maxval(t_ppm data, uint16_t *maxval);
int			ft_ppm_set_maxval(t_ppm *data, uint16_t maxval);

int			ft_ppm_get_plainformat(t_ppm data, int *plainformat);
int			ft_ppm_set_plainformat(t_ppm *data, int plainformat);

#endif
