/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_ppm.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 19:55:24 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/06 17:30:28 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_PPM_H
# define T_PPM_H

struct	s_ppm
{
	int			file;
	int			width;
	int			height;
	int			plain;
	uint16_t	maxval;
	int			bytes_per_sample;
};

typedef struct s_ppm	t_ppm;

#endif
