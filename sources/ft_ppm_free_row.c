/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ppm_free_row.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 16:07:24 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/06 17:40:46 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_ppm.h"

void	ft_ppm_free_row(uint16_t **row, t_ppm data)
{
	int	i;

	i = 0;
	while (i < data.width)
		free(row[i++]);
	free(row);
}
