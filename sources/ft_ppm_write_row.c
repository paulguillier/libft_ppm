/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ppm_write_row.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/03 11:17:07 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/08 14:09:18 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_ppm.h"

static int		ft_ppm_write_row_plain(t_ppm data, uint16_t **row)
{
	int	i;
	int	j;

	i = 0;
	while (i < data.width)
	{
		j = 0;
		while (j < 3)
		{
			if (row[i][j] > data.maxval)
				return (-1);
			if (ft_putnbr_fd(row[i][j], data.file) < 0)
				return (-1);
			if (write(data.file, "\n", 1) < 0)
				return (-1);
			j++;
		}
		i++;
	}
	return (0);
}

static uint8_t	*ft_ppm_write_row_raw_one(uint16_t **row, int width)
{
	uint8_t	*buf;
	int		i;
	int		j;

	if ((buf = malloc(width * 3)))
	{
		i = 0;
		while (i < width)
		{
			j = 0;
			while (j < 3)
			{
				buf[i * 3 + j] = row[i][j];
				j++;
			}
			i++;
		}
	}
	return (buf);
}

static uint8_t	*ft_ppm_write_row_raw_two(uint16_t **row, int width)
{
	uint8_t	*buf;
	int		i;
	int		j;

	if ((buf = malloc(2 * width * 3)))
	{
		i = 0;
		while (i < width)
		{
			j = 0;
			while (j < 3)
			{
				ft_memcpy(buf + 2 * i * 3 + j, (void *)(row[i] + j) + 1, 1);
				ft_memcpy(buf + 2 * i * 3 + j + 1, row[i] + j, 1);
				j++;
			}
			i++;
		}
	}
	return (buf);
}

static int		ft_ppm_write_row_raw(t_ppm data, uint16_t **row)
{
	uint8_t	*buf;
	int		i;
	int		len;

	i = 0;
	if (data.bytes_per_sample == 1)
	{
		if ((buf = ft_ppm_write_row_raw_one(row, data.width)) == NULL)
			return (-1);
		len = data.width * 3;
	}
	else
	{
		if ((buf = ft_ppm_write_row_raw_two(row, data.width)) == NULL)
			return (-1);
		len = data.width * 2;
	}
	if (write(data.file, buf, len) < 0)
	{
		free(buf);
		return (-1);
	}
	free(buf);
	return (0);
}

int				ft_ppm_write_row(t_ppm data, uint16_t **row)
{
	if (data.plain)
		return (ft_ppm_write_row_plain(data, row));
	return (ft_ppm_write_row_raw(data, row));
}
