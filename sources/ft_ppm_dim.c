/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ppm_dim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 15:58:35 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/06 17:09:08 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_ppm.h"

int	ft_ppm_get_dim(t_ppm data, int *width, int *height)
{
	if (width)
		*width = data.width;
	if (height)
		*height = data.height;
	if (!data.width || !data.height)
		return (-1);
	return (0);
}

int	ft_ppm_set_dim(t_ppm *data, int width, int height)
{
	if (data == NULL)
		return (-1);
	if (width)
		data->width = width;
	if (height)
		data->height = height;
	return (0);
}
