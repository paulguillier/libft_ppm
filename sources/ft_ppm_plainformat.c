/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ppm_plainformat.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 18:32:55 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/06 17:09:08 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_ppm.h"

int	ft_ppm_get_plainformat(t_ppm data, int *plainformat)
{
	if (plainformat)
		*plainformat = data.plain;
	return (data.plain);
}

int	ft_ppm_set_plainformat(t_ppm *data, int plainformat)
{
	if (data == NULL)
		return (-1);
	data->plain = !(!plainformat);
	return (data->plain);
}
