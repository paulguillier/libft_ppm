/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ppm_maxval.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 14:22:33 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/06 17:30:28 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_ppm.h"

int	ft_ppm_get_maxval(t_ppm data, uint16_t *maxval)
{
	if (maxval)
		*maxval = data.maxval;
	if (!data.maxval)
		return (-1);
	return (0);
}

int	ft_ppm_set_maxval(t_ppm *data, uint16_t maxval)
{
	if (data == NULL)
		return (-1);
	if (maxval)
	{
		data->maxval = maxval;
		data->bytes_per_sample = (maxval < 256 ? 1 : 2);
	}
	return (0);
}
