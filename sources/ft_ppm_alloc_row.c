/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ppm_alloc_row.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 16:00:58 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/08 11:01:05 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_ppm.h"

uint16_t	**ft_ppm_alloc_row(t_ppm data)
{
	uint16_t	**row;
	int			i;

	if ((row = (uint16_t **)malloc(sizeof(uint16_t *) * data.width)))
	{
		i = 0;
		while (i < data.width)
		{
			if ((row[i] = ((uint16_t *)malloc(sizeof(uint16_t) * 3))) == NULL)
			{
				while (i--)
					free(row[i]);
				free(row);
				row = NULL;
				break ;
			}
			i++;
		}
	}
	return (row);
}
